## About b45-task

b45-task was made for my learning process in applying simple backend javascript programming. At the same time to fulfill the assignment given for the job application that I sent.

## About task

- task1: (1. Tuliskan perhitungan 6x7-5+10÷2 dengan menggunakan javascript )
- task2: (2. Buatkan script menampilkan bilangan fibonaci, bebas dengan menggunakan bahasa yang paling anda kuasai)
- task3: (3. Saya membeli barang pada suatu sistem, dengan menggunakan saldo saya yang terdaftar di sistem, buatlah sebuah flowchart yang menggambarkan proses tersebut)

## Document task
- **[DOCX](https://gitlab.com/b45-prototype/b45-task/commit/1c153755f32f45462a37ed1a794ac2512f3f91f4)**

## Task implementation

Use internal javascript (tag script inside html file)
- task1: Operator aritmatika with operator precedence

Use externaal javascript (call script from different file)
Fibbonacci start from 0 (0 1 1 2 3 5 ...)
Showing fibonacci_series and last number of fibonacci
- task2:Basic logic 

Use visio (flowchart)
- task3:System Analyst with flowchart

## Program outline 

- **[task1](https://gitlab.com/b45-prototype/b45-task/commit/d18d7869e7d3672e5fc3a952f6f0077e2df303d7)**
- **[task2](https://gitlab.com/b45-prototype/b45-task/commit/ec3ef90b7c136fd521643ad87c8fb414f0234b40)**
- **[task3](https://gitlab.com/b45-prototype/b45-task/commit/e53b3c39fc23cbfc170ee72aaefbc35d57594219)**