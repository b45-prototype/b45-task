function fibonacci(num){
    var a = 0, b = 1, temp;

    var fibonacci_series = function (num) 
    {
      if (num===1) 
      {
        return [0, 1];
      } 
      else 
      {
        var s = fibonacci_series(num - 1);
        s.push(s[s.length - 1] + s[s.length - 2]);
        return s;
      }
    };
    
    console.log(fibonacci_series(num));

    while (num >= 0){
      temp = a;
      a = a + b;
      b = temp;
      num--;
    }  
    return b;
  }

